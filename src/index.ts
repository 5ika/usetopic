import { useRef, useEffect, useReducer } from "react";
import mqtt, { MqttClient } from "mqtt/dist/mqtt";

const useTopic = (mqttURL: string, topic: string, defaultValue?: string[]) => {
  let client = useRef<MqttClient | null>(null);
  const [messages, addMessage] = useReducer(reducer, defaultValue || []);

  useEffect(() => {
    if (!client.current) {
      client.current = mqtt.connect(mqttURL);
      client.current?.on("connect", () => {
        console.log(`Connected to ${mqttURL}`);
        client.current?.subscribe(topic, err => {
          if (err) return console.error(err);
          console.log(`Subscribed to "${topic}"`);
        });
      });

      client.current.on("message", (_receieveTopic, rawMessage) => {
        const message = rawMessage.toString();
        addMessage(message);
      });
    }
  });

  const sendMessage = (message: string) => {
    if (client.current) client.current.publish(topic, message);
  };

  return { messages, sendMessage };
};

const reducer = (messages: string[], newMessage: string) => [
  ...messages,
  newMessage,
];

export default useTopic;
