declare const useTopic: (mqttURL: string, topic: string, defaultValue?: string[]) => {
    messages: string[];
    sendMessage: (message: string) => void;
};
export default useTopic;
