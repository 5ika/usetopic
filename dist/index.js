import { useRef, useEffect, useReducer } from "react";
import mqtt from "mqtt/dist/mqtt";
const useTopic = (mqttURL, topic, defaultValue) => {
    let client = useRef(null);
    const [messages, addMessage] = useReducer(reducer, defaultValue || []);
    useEffect(() => {
        var _a;
        if (!client.current) {
            client.current = mqtt.connect(mqttURL);
            (_a = client.current) === null || _a === void 0 ? void 0 : _a.on("connect", () => {
                var _a;
                console.log(`Connected to ${mqttURL}`);
                (_a = client.current) === null || _a === void 0 ? void 0 : _a.subscribe(topic, err => {
                    if (err)
                        return console.error(err);
                    console.log(`Subscribed to "${topic}"`);
                });
            });
            client.current.on("message", (_receieveTopic, rawMessage) => {
                const message = rawMessage.toString();
                addMessage(message);
            });
        }
    });
    const sendMessage = (message) => {
        if (client.current)
            client.current.publish(topic, message);
    };
    return { messages, sendMessage };
};
const reducer = (messages, newMessage) => [
    ...messages,
    newMessage,
];
export default useTopic;
