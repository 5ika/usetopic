# useTopic

## Getting started

```javascript
import React, { useState } from "react";
import useTopic from "usetopic";

const MQTT_SERVER = "ws://test.mosquitto.org:8080";
const TOPIC = "mytopic";

const Feed = () => {
  const { messages, sendMessage } = useTopic(MQTT_SERVER, TOPIC);
  const [value, setValue] = useState("");

  const send = () => {
    sendMessage(value);
    setValue("");
  };

  return (
    <div>
      {messages.map((message, index) => (
        <div key={index}>{message}</div>
      ))}
      <input value={value} onChange={e => setValue(e.target.value)} />
      <button onClick={send}>Send</button>
    </div>
  );
};
```

> Following MQTT.js documentation, _Your broker should accept websocket connection_ in order to use it in a browser.

## Documentation

- [MQTT.js](https://github.com/mqttjs/MQTT.js#readme)
- [React Hooks](https://reactjs.org/docs/hooks-intro.html)
